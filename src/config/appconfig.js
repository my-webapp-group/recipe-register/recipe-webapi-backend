const configs = {
  production: {
    jwtSecret: "secret",
    port: process.env.PORT,
    aws:{
      accessKey: process.env.AWS_ACCESS_KEY_ID,
      secretKey: process.env.AWS_SECRET_ACCESS_KEY,
      s3Bucket: process.env.AWS_IMAGE_BUCKET_NAME,
      s3BucketRegion : process.env.AWS_IMAGE_BUCKET_REGION
    },
  },
  development: {
    jwtSecret: "secret",
    port: 8081,
  },
  test: {
    jwtSecret: "secret",
    port: 8082,
  }
}

module.exports = configs[process.env.NODE_ENV]