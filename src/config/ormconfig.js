const configs = {
  development: {
    type: "postgres",
    host: "localhost",
    port: 52999,
    database: "./db/test/test.db",
    synchronize: false,
    logging: false,
    entities: [
      "src/models/**/*.ts",
    ],
    migrations: [
      "src/migration/*.ts"
    ],
    subscriber: [
      "src/subscriber/**/*.ts"
    ],
    cli: {
      "entitiesDir": "src/models",
      "migrationsDir": "src/migration",
      "subscribersDir": "src/subscriber",
    }
  },
  production: {
    type: "postgres",
    host: process.env.RDS_HOSTNAME,
    port: Number.parseInt(process.env.RDS_PORT),
    database: process.env.RDS_DB_NAME,
    username: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD,
    synchronize: false,
    logging: false,
    entities: [
      "src/models/**/*.ts",
    ],
    migrations: [
      "src/migration/*.ts"
    ],
    subscribers: [
      "src/subscriber/**/*.ts"
    ],
    cli: {
      "entitiesDir": "src/models",
      "migrationsDir": "src/migration",
      "subscribersDir": "src/subscriber",
    }
  },
  test: {
    type: "sqlite",
    host: "localhost",
    database: ":memory:",
    synchronize: true,
    logging: false,
    entities: [
      "src/models/**/*.{js,ts}",
    ],
    migrations: [
      "src/migration/*.ts"
    ],
    subscribers: [
      "src/subscriber/**/*.ts"
    ],
    cli: {
      "entitiesDir": "src/models",
      "migrationsDir": "src/migration",
      "subscribersDir": "src/subscriber",
    }
  },

}

module.exports = configs[process.env.NODE_ENV]