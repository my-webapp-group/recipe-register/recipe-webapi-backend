import { v4 as uuidV4 } from "uuid";
import AWS from "aws-sdk";
import S3 from "aws-sdk/clients/s3";
import {PromiseResult} from "aws-sdk/lib/request";

AWS.config.logger = console; // 通信のデバッグ用。不要であれば削除可。
const configs = require('../config/appconfig');

const bucket = new S3({
    accessKeyId: configs.accessKeyId,// IAMユーザの認証情報の「アクセスキーID」から確認できます。
    secretAccessKey: configs.secretAccessKey,// IAMユーザのシークレットアクセスキー。アクセスキーを作ったときだけ見れるやつです。
    region: configs.region,
});

export const uploadFile = (file: Express.Multer.File) => {
    const myFile = file.originalname.split(".");
    const fileType = myFile[myFile.length - 1];
    const uploadParams: S3.PutObjectRequest = {
        Bucket: configs.bucketName,// 保存先のバケット名
        Key: `${uuidV4()}.${fileType}`, // S3内で使用されるKey名
        Body: file.buffer, // ファイルの内容
        ContentType: file.mimetype,
    };
    return bucket.upload(uploadParams).promise();
};

export const fetchFile = (fileKey: string): Promise<PromiseResult<S3.GetObjectOutput, AWS.AWSError>> => {
  const fetchParams: S3.GetObjectRequest = {
    Bucket: configs.bucketName,
    Key: fileKey
  };
  return bucket.getObject(fetchParams).promise();
};
