import * as express from 'express';
import loaders from './loaders';

async function main() {
  const app = express.default();
  await loaders(app);
  const configs = require('./config/appconfig');

  app.listen(configs.port, () => {
    console.log(`Server listening on port: ${configs.port}`);
  });
}

main();
