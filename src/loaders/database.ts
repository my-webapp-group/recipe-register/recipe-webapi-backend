import Container from "typedi";
import { Connection, createConnection, useContainer } from "typeorm";
import { PostgresConnectionOptions } from "typeorm/driver/postgres/PostgresConnectionOptions";
import { User } from "../models/user/User";
import { Recipe } from "../models/recipe/Recipe";
import { Flow } from "../models/recipe/Flow";
import { MainImage } from "../models/recipe/MainImage";
import { Material } from "../models/recipe/Material";

export default async (): Promise<Connection> => {
    // read connection options from ormconfig file (or ENV variables)
    // const connectionOptions = await getConnectionOptions();
    const configs = require('../config/ormconfig');
    const connectionOptions: PostgresConnectionOptions = {
        type: configs.type,
        host: configs.host,
        port: configs.port,
        database: configs.database,
        username: configs.username,
        password: configs.password,
        synchronize: configs.synchronize,
        logging: configs.logging,

        /**
         * @TODO Should set migration step
         */
        entities: [User, Recipe, MainImage, Material, Flow],
        // cli: {
        //     migrationsDir: "src/models/migration",
        // },
    };

    // typedi + typeorm
    useContainer(Container);

    // create a connection using modified connection options
    const connection = await createConnection(connectionOptions);

    return connection;
};
