FROM node:14.15.1

# Create work directory
WORKDIR /usr/src/app

# package.jsonとyarn.lockを/usr/src/appにコピー
COPY ["package.json", "yarn.lock","./"]

# パッケージをインストール
RUN yarn install

# ローカル作業フォルダ内のファイルを全て/usr/src/appにコピー
COPY . /usr/src/app

# ポートの開放
EXPOSE 8080

# コンテナを起動する際に実行されるコマンド
ENTRYPOINT [ "npm", "run", "start:prod" ]
